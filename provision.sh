#!/bin/bash

# IGN_CONFIG=''
# IMAGE=''
# VM_NAME=''
# VCPUS=''
# RAM_MB=''
# DISK_GB=''
# STREAM=''

tree -d manifests/
echo "Enter <hostname>:"
read host_path
source manifests/$host_path/fcos.env


sudo -E chcon --verbose --type svirt_home_t ${IGN_CONFIG}
sudo -E virt-install --connect="qemu:///system" --name="${VM_NAME}" \
    --vcpus="${VCPUS}" --memory="${RAM_MB}" \
    --os-variant="fedora-coreos-$STREAM" --import --graphics=none \
    --disk="size=${DISK_GB},backing_store=${IMAGE}" \
    --qemu-commandline="-fw_cfg name=opt/com.coreos/config,file=${IGN_CONFIG}" \
    --network network=default \
    --autostart 
    #--dry-run
