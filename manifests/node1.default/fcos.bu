variant: fcos
version: 1.4.0
storage:
  files:
    # CRI-O DNF module
    - path: /etc/dnf/modules.d/cri-o.module
      mode: 0644
      overwrite: true
      contents:
        inline: |
          [cri-o]
          name=cri-o
          stream=1.17
          profiles=
          state=enabled
    # YUM repository for kubeadm, kubelet and kubectl
    - path: /etc/yum.repos.d/kubernetes.repo
      mode: 0644
      overwrite: true
      contents:
        inline: |
          [kubernetes]
          name=Kubernetes
          baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
          enabled=1
          gpgcheck=0
          repo_gpgcheck=0
          gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg
    # configuring automatic loading of br_netfilter on startup
    - path: /etc/modules-load.d/br_netfilter.conf
      mode: 0644
      overwrite: true
      contents:
        inline: br_netfilter
    # setting kernel parameters required by kubelet
    - path: /etc/sysctl.d/kubernetes.conf
      mode: 0644
      overwrite: true
      contents:
        inline: |
          net.bridge.bridge-nf-call-iptables=1
          net.ipv4.ip_forward=1
    # Kube preps end

    # Set the Hostname
    - path: /etc/hostname
      overwrite: true
      mode: 0644
      contents:
        inline: |
          node1.default

    # Set motd
    - path: /etc/motd
      mode: 0644
      overwrite: true
      contents:
        source: https://raw.githubusercontent.com/tchnmf/fcos-ops/main/motd

    # Copy GPG key
    # - path: /var/home/core/.gpg.key
    #   contents:
    #     local ./gpg.key
    #   mode: 0600
    #   user:
    #     id: 1000
    #   group:
    #     id: 1000

    # Copy DO token shell profile export
    - path: /etc/profile.d/do-token.sh
      contents:
        local: ./do-token.sh
      mode: 0755

    # Copy kube-installation script
    - path: /usr/local/bin/kube-install
      mode: 0755
      contents:
        # source: https://raw.githubusercontent.com/tchnmf/fcos-ops/feature/scripts/kube-install
        local: ./kube-install

    # Copy dns-record.sh script
    - path: /usr/local/bin/dns-record
      mode: 0755
      contents:
        source: https://raw.githubusercontent.com/tchnmf/fcos-ops/feature/scripts/dns-record.sh

systemd:
  units:
    # # Kube installation
    - name: kube-install.service
      enabled: true
      contents: |
        [Unit]
        Wants=network-online.target
        After=network-online.target
        Description=Kube installation unit!
        [Service]
        Type=oneshot
        RemainAfterExit=yes
        ExecStart=/usr/local/bin/kube-install
        [Install]
        WantedBy=multi-user.target

    # DNS record
    - name: dns-record.service
      enabled: true
      contents: |
        [Unit]
        Wants=network-online.target
        After=network-online.target
        Description=DNS host record unit!
        [Service]
        Type=oneshot
        RemainAfterExit=yes
        ExecStart=/usr/local/bin/dns-record
        [Install]
        WantedBy=multi-user.target

passwd: # setting login credentials
  users:
    - name: core
      ssh_authorized_keys:
        - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAII9UPJZAwZVt+OrgtTeuAK100qRnXJ1HwEsfX/imU3hF fthufuss@t470.brq
        - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBTTEqf1adihi10USTdI+frH+F2j1mrhxfowI1DfGj2V fthufuss@alpine
