#!/bin/bash

vm="$1"
echo $vm

function remove_vm() {
    sudo virsh destroy $vm
    sudo virsh undefine --remove-all-storage $vm
}


remove_vm
